// config/auth.js

// expose our config directly to our application using module.exports
module.exports = {
    
        'facebookAuth' : {
            'clientID'      : '134234887352755', // your App ID
            'clientSecret'  : '624e10f6fa6c6c9f5ee9691ba20e4f1e', // your App Secret
            'callbackURL'   : 'http://127.0.0.1:8080/auth/facebook/callback',
            'profileFields'   : ['emails','name']
        },
    
        'twitterAuth' : {
            'consumerKey'       : 'your-consumer-key-here',
            'consumerSecret'    : 'your-client-secret-here',
            'callbackURL'       : 'http://localhost:8080/auth/twitter/callback'
        },
    
        'googleAuth' : {
            'clientID'      : '1087431159499-onhbmk44uaqh40rmu9g0donp000snrrp.apps.googleusercontent.com',
            'clientSecret'  : 'vPCZqYOdnrOv5h0Ug45_S9EV',
            'callbackURL'   : 'http://127.0.0.1:8080/auth/google/callback'
        }
    
    };